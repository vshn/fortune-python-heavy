# Inherit from this "empty base image", see https://hub.docker.com/_/python/
FROM python:3.10-alpine

# Take some responsibility for this container
MAINTAINER Jane Smith <jane.smith@example.com>

# Install some required software on Alpine Linux
RUN apk add fortune

# Directory to install the app inside the container
WORKDIR /usr/src/app

# Install python dependencies (cached if requirements.txt does not change)
COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

# Copy application source code into container
COPY app.py /usr/src/app
COPY templates /usr/src/app/templates/

# Drop root privileges when running the application
USER 1001

# Run this command at run-time
CMD [ "python", "app.py" ]

# Expose this TCP-port, same as app.py
EXPOSE 8080
